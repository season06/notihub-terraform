provider "aws" {
  access_key = var.access_key
  secret_key = var.secret_key
  region     = var.region
}



# EventBridge
resource "aws_cloudwatch_event_bus" "eventbus" {
  name = "custom"
}

resource "aws_cloudwatch_event_permission" "bus_policy" {
  event_bus_name = aws_cloudwatch_event_bus.eventbus.name
  principal      = var.client_account
  statement_id   = "AccountAccess"
}

resource "aws_cloudwatch_event_rule" "eventbus_rule" {
  event_bus_name = aws_cloudwatch_event_bus.eventbus.name
  name           = "all_cloudwatch_event"
  description    = "capture all cloudwatch alarm"

  event_pattern = jsonencode({ "source" : ["aws.cloudwatch"] })
}

resource "aws_cloudwatch_event_target" "rule_target" {
  event_bus_name = aws_cloudwatch_event_bus.eventbus.name
  rule           = aws_cloudwatch_event_rule.eventbus_rule.name
  arn            = var.eventbus_rule_target
}



# DynamoDB
resource "aws_dynamodb_table" "notihub_table" {
  name           = "notihub"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "account_id"
  range_key      = "alarm_name"

  attribute {
    name = "account_id"
    type = "S"
  }

  attribute {
    name = "alarm_name"
    type = "S"
  }
}
resource "aws_dynamodb_table" "notihub_log_table" {
  name           = "notihub_log"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "account_id"
  range_key      = "alarm_name"

  attribute {
    name = "account_id"
    type = "S"
  }

  attribute {
    name = "alarm_name"
    type = "S"
  }
}
resource "aws_dynamodb_table" "notihub_prtg_table" {
  name           = "notihub_prtg"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "account_id"

  attribute {
    name = "account_id"
    type = "S"
  }
}



# Lambda
module "lambda_prtg" {
  source = "terraform-aws-modules/lambda/aws"

  create_current_version_allowed_triggers = false
  create_role                             = false

  function_name = "notihub-prtg"
  description   = "Notihub Prtg Log"
  runtime       = "python3.8"
  memory_size   = "128"
  timeout       = "180"
  handler       = "prtg_entry.main"
  source_path   = "./src/prtg_entry.py"

  environment_variables = {
    REGION      = var.region
    SECRET_NAME = var.secret_name
  }

  lambda_role = aws_iam_role.lambda_role.arn
}


# lambda - iam role
resource "aws_iam_role" "lambda_role" {
  name               = "notihub_cms_lambda_role"
  assume_role_policy = file("./iam_policy/lambda_role.json")
}

resource "aws_iam_policy" "lambda_policy" {
  name   = "notihub_cms_lambda_policy"
  policy = file("./iam_policy/lambda_policy.json")
}

resource "aws_iam_policy_attachment" "lambda_attach" {
  name       = "lambda_attachment"
  roles      = [aws_iam_role.lambda_role.name]
  policy_arn = aws_iam_policy.lambda_policy.arn
}




# Secret Manager
resource "aws_secretsmanager_secret" "secret_manager" {
  name = "notihub_secret"
}

resource "aws_secretsmanager_secret_version" "secret_value" {
  secret_id     = aws_secretsmanager_secret.secret_manager.id
  secret_string = jsonencode(var.secret_value)
}



# S3 Bucket
resource "aws_s3_bucket" "s3_bucket" {
    bucket = var.s3_bucket_name
    acl = "public-read"
}

resource "aws_s3_bucket_policy" "bucket_policy" {
    bucket = aws_s3_bucket.s3_bucket.id
    policy = jsonencode({
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "PublicRead",
                "Effect": "Allow",
                "Principal": "*",
                "Action": [
                    "s3:GetObject",
                    "s3:GetObjectVersion"
                ],
                "Resource": "${aws_s3_bucket.s3_bucket.arn}/*"
            }
        ]
    })
}



# SNS -> lambda
resource "aws_sns_topic" "sns_topic" {
  name = "prtg-sns"
}

resource "aws_sns_topic_subscription" "sns_subsription" {
  topic_arn = aws_sns_topic.sns_topic.arn
  protocol  = "lambda"
  endpoint  = module.lambda_prtg.lambda_function_arn
}