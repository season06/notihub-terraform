output "event_bus_name" {
    value = aws_cloudwatch_event_bus.eventbus.name
}

output "event_bus_rule_name" {
    value = aws_cloudwatch_event_rule.eventbus_rule.name
}

output "lambda_prtg_arn" {
  value = module.lambda_prtg.lambda_function_arn
}

output "secret_manage_name" {
    value = aws_secretsmanager_secret.secret_manager.name
}

output "s3_bucket_arn" {
    value = aws_s3_bucket.s3_bucket.arn
}

output "sns_topic_name" {
    value = aws_sns_topic.sns_topic.name
}