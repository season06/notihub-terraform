import os
import json
import time
import boto3
from botocore.exceptions import ClientError

"""
event = {
  "Records": [
    {
      "EventSource": "aws:sns",
      "EventVersion": "1.0",
      "EventSubscriptionArn": "arn:aws:sns:us-west-1:863936362823:season-sns:dfcb81c5-71fc-444b-af85-7655a14d1510",
      "Sns": {
        "Type": "Notification",
        "MessageId": "a0fdb897-0db4-5e46-980b-e19de307f3d0",
        "TopicArn": "arn:aws:sns:us-west-1:863936362823:season-sns",
        "Subject": "prtg-test",
        "Message": "{     \"account_id\": \"123e456789\",     \"sitename\": \"MSP-PRTG Monitor\",     \"device\": \"Device\",     \"name\": \"HTTP (HTTP)\",     \"status\": \"Down ended (now: Up)\",     \"down\": \"(Downtime: 30 m 31 s)\",     \"message\": \"OK\",     \"host\": \"http://season-lb-677244504.us-west-1.elb.amazonaws.com/\" }",
        "Timestamp": "2021-05-04T06:40:51.681Z",
        "SignatureVersion": "1",
        "Signature": "bgtzqxG39YlYFuaQ/NSGYppoVXauSeEGVOPzHtBr80zFntP49VcMgv44pKWoXTbb2w68QVc+QouEhPNMtLGbbIX9L9M7klCOpMrFppHGmM5g5wyDouiGZj7fw90tT8cBzzXuthnIwoPYKXAwDLfiVlX5cSVL67NIVpY1nZvZAO3q+mIrzD9CSxtLsPYL5nJPQKlb+yt2M9FzNdUhPdN++r6EziNSrwHy2gG2s5ZNeM/cjXhqp59imAUYaX/2NvpM7CblHgREN1Vs4KGATvIHlemLfvkRRn4ncyEJ37WN07bXDJmuHNICiBCFK+IU274MKIFTu6wW7XA8nD7cSHwbFg==",
        "SigningCertUrl": "https://sns.us-west-1.amazonaws.com/SimpleNotificationService-010a507c1833636cd94bdb98bd93083a.pem",
        "UnsubscribeUrl": "https://sns.us-west-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-1:863936362823:season-sns:dfcb81c5-71fc-444b-af85-7655a14d1510",
        "MessageAttributes": {}
      }
    }
  ]
}
"""

def get_secret_manager():
    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=os.environ['REGION']
    )

    get_secret_value_response = client.get_secret_value(
        SecretId=os.environ['SECRET_NAME']
    )

    secret = get_secret_value_response['SecretString']
    return json.loads(secret)


class DynamodbAction(object):
    def __init__(self, item):
        self.item = item
        self.primary_key = {
            'account_id': item['account_id']
        }

        secret_param = get_secret_manager()
        dynamodb = boto3.resource(
            'dynamodb',
            region_name=os.environ['REGION'],
            aws_access_key_id=secret_param['aws_access_key_id'],
            aws_secret_access_key=secret_param['aws_secret_access_key']
        )
        self.table = dynamodb.Table('notihub_prtg')

    def create_item(self):
        self.table.put_item(Item=self.item)

    def get_item(self):
        response = self.table.get_item(Key=self.primary_key)

        return response.get('Item')

    def delete_item(self):
        response = self.table.delete_item(Key=self.primary_key)
        print('delete success')


def main(event, context):
    try:
        # analysis prtg info
        message = event['Records'][0]['Sns']['Message']
        msg = json.loads(message)
        item = {
            'prtg_info': msg,
            'account_id': msg['account_id'],
            'subject': event['Records'][0]['Sns']['Subject'],
            'time': event['Records'][0]['Sns']['Timestamp'],
            'stage': 'prtg'
        }

        ddb_action = DynamodbAction(item)

        # check if account_id is exist
        item = ddb_action.get_item()
        if item:
            print('This account alarm already exists -> end')
            return {
                'statusCode': 200,
                'body': json.dumps('This account alarm already exists')
            }

        # create a record
        ddb_action.create_item()

        # wait for cloudwatch update
        time.sleep(60)

        # check flow stage
        item = ddb_action.get_item()
        if item['stage'] == 'cloudwatch':
            ddb_action.delete_item()
        else:
            print('send message to MSP')

        return {
            'statusCode': 200,
            'body': json.dumps('prtg flow success')
        }
    except ClientError as e:
        print(e.response['Error']['Message'])

        return {
            'statusCode': 500,
            'body': json.dumps(e.response['Error']['Message'])
        }